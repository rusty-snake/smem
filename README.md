smem
====

Show top 10 memory consuming processes.

Getting started
---------------

Install [scala-cli](https://scala-cli.virtuslab.org/install) and run it.

```console
$ ./smem.sc
Name                 Pid      VmRSS     VmSwap
qemu-system-x86   121497  774,97 MB 1411,70 MB
java              133959  756,38 MB   87,25 MB
brave             124258  190,34 MB  471,92 MB
brave             124691  493,24 MB   98,47 MB
firefox           134443  420,32 MB   91,10 MB
java              134129  450,21 MB   14,13 MB
gnome-shell         2622  140,63 MB   87,49 MB
Isolated Web Co   137990  193,45 MB    0,00 MB
Isolated Web Co   137910  165,96 MB    0,00 MB
WebExtensions     134841  155,53 MB    6,75 MB
```

For faster execution you can compile it with scala-native and add it to `$PATH`.

```bash
scala-cli --power package --native-image smem.sc
mv smem ~/.local/bin/smem
```
